#!/bin/bash
cd $(dirname $0) && cd ../..

rm -rf ./images/about/src
cp -r ../src ./images/about/src
(cd ./images/about/src && meteor reset >/dev/null)

# Load Environment Variables
set -a
source variables.default.env
if [ -f variables.env ]; then
    source variables.env
fi

# Run Docker Compose
docker-compose build --build-arg GITHUB_TOKEN=$GITHUB_TOKEN $1
rm -rf ./images/about/src
