#!/bin/bash
cd $(dirname $0) && cd ../..

# Load Environment Variables
set -a
source variables.default.env
if [ -f variables.env ]; then
    source variables.env
fi

# Run Docker Compose
./scripts/docker/build-image.sh about

# Run the new Image
docker-compose up --no-deps -d about
