import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter, StaticRouter } from 'react-router-dom';

import Routes from '/both/imports/routes';

class App extends Component {
  render() {
    return <Routes />;
  }
}
App.propTypes = {
  location: PropTypes.object,
};

export { App };
