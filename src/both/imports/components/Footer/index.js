import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import {
  FooterStyle,
  DividerStyle,
  LegalContainer,
  FooterLink,
  FooterLinkA,
  ContactInformationContainer,
} from './styles';

class _Footer extends Component {
  state = {};
  render() {
    // const { pathname } = this.props.location;
    return (
      <FooterStyle>
        <FooterLink to="/about">About</FooterLink>
        <DividerStyle>|</DividerStyle>
        <FooterLinkA href="https://blog.atomsandbits.ai">Blog</FooterLinkA>
        <DividerStyle>|</DividerStyle>
        <FooterLinkA href="https://discuss.atomsandbits.ai">
          Discuss
        </FooterLinkA>
        <DividerStyle>|</DividerStyle>
        <FooterLinkA href="https://playground.atomsandbits.ai">
          Playground
        </FooterLinkA>
        <DividerStyle>|</DividerStyle>
        <FooterLink to="/technology">Technology</FooterLink>
        <DividerStyle>|</DividerStyle>
        <FooterLinkA href="https://quantum.atomsandbits.ai">
          Quantum
        </FooterLinkA>
        <ContactInformationContainer id="contact">
          <FooterLinkA href="email:contact@atomsandbits.ai">Email</FooterLinkA>
          <DividerStyle>|</DividerStyle>
          <FooterLinkA href="https://gitlab.com/atomsandbits/atomsandbits">
            GitLab
          </FooterLinkA>
        </ContactInformationContainer>
        <LegalContainer>
          © 2018<FooterLinkA href="/">Atoms+Bits</FooterLinkA>
        </LegalContainer>
      </FooterStyle>
    );
  }
}
_Footer.propTypes = {
  location: PropTypes.shape({ pathname: PropTypes.string }),
};

const Footer = withRouter(_Footer);

export { Footer };
