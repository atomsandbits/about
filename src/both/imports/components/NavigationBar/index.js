import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import FaMenu from 'react-icons/lib/md/menu';

import {
  MenuButton,
  MenuStyle,
  MenuLink,
  MenuLinkA,
  TextLink,
  TextLinkA,
  Logo,
  NavigationBarStyle,
} from './styles';

class _NavigationBar extends Component {
  state = {
    navigationOpen: false,
  };
  componentDidMount() {
    window.addEventListener('click', this.handleDocumentClick.bind(this));
  }
  componentWillUnmount() {
    window.removeEventListener('click', this.handleDocumentClick);
  }
  handleDocumentClick(event) {
    this.setState({
      navigationOpen: false,
    });
  }
  handleMenuClick(event) {
    event.stopPropagation();
    this.setState({
      navigationOpen: !this.state.navigationOpen,
    });
  }
  render() {
    // const { pathname } = this.props.location;
    return (
      <NavigationBarStyle height="64px">
        <Link to="/">
          <Logo src="/logo.svg" />
        </Link>
        <div style={{ flexGrow: 1 }} />
        <MenuButton onClick={this.handleMenuClick.bind(this)}>
          <FaMenu
            style={{
              fontSize: '1.5rem',
              color: 'inherit',
            }}
          />
        </MenuButton>
        <TextLink to="/about">About</TextLink>
        <TextLinkA href="https://blog.atomsandbits.ai">Blog</TextLinkA>
        <TextLinkA href="https://discuss.atomsandbits.ai">Discuss</TextLinkA>
        <TextLinkA href="https://playground.atomsandbits.ai">
          Playground
        </TextLinkA>
        <TextLink to="/technology">Technology</TextLink>
        {this.state.navigationOpen ? (
          <MenuStyle>
            <ul>
              <li>
                <MenuLink to="/about">About</MenuLink>
              </li>
              <li>
                <MenuLinkA href="https://blog.atomsandbits.ai">Blog</MenuLinkA>
              </li>
              <li>
                <MenuLinkA href="https://discuss.atomsandbits.ai">Discuss</MenuLinkA>
              </li>
              <li>
                <MenuLinkA href="https://playground.atomsandbits.ai">
                  Playground
                </MenuLinkA>
              </li>
              <li>
                <MenuLink to="/technology">Technology</MenuLink>
              </li>
            </ul>
          </MenuStyle>
        ) : null}
      </NavigationBarStyle>
    );
  }
}

const NavigationBar = withRouter(_NavigationBar);

export { NavigationBar };
