import React from 'react';
import { compose, withProps, lifecycle } from 'recompose';
import { Meteor } from 'meteor/meteor';

import { NavigationBar } from '/both/imports/components/NavigationBar';
import { Footer } from '/both/imports/components/Footer';
import { RootStyle, ContainerStyle } from '/both/imports/styles';
import {
  Description,
  FirstSection,
  StartLink,
  SplashTextContainer,
  Title,
  StarField,
  Square,
  Circle,
} from '../Splash/styles';

let About;
if (Meteor.isClient) {
  import { VelocityComponent } from 'velocity-react';
  const AboutPure = ({ getShapeSize, shouldBlink }) => (
    <RootStyle>
      <ContainerStyle>
        <FirstSection>
          <NavigationBar />
          <SplashTextContainer>
            <Title src="/brand-thin.png" />
            <Description>under construction!</Description>
          </SplashTextContainer>
        </FirstSection>
        <StarField>
          {new Array(15).fill(0).map((value, index) => {
            const x = Math.random();
            const y = Math.random();
            return (
              <VelocityComponent
                key={`square-${index}`}
                animation={{ opacity: 0.8 }}
                duration={Math.floor(Math.random() * 2000) + 3000}
                delay={Math.floor(Math.random() * 3500) + 1000}
                loop={true}
                runOnMount={true}
              >
                <Square size={getShapeSize(x, y)} random={x} randomTwo={y} />
              </VelocityComponent>
            );
          })}
          {new Array(15).fill(0).map((value, index) => {
            const x = Math.random();
            const y = Math.random();
            return (
              <VelocityComponent
                key={`circle-${index}`}
                animation={{ opacity: 0.8 }}
                duration={Math.floor(Math.random() * 2000) + 3000}
                delay={Math.floor(Math.random() * 3500) + 1000}
                loop={true}
                runOnMount={true}
              >
                <Circle size={getShapeSize(x, y)} random={x} randomTwo={y} />
              </VelocityComponent>
            );
          })}
        </StarField>
        <Footer />
      </ContainerStyle>
    </RootStyle>
  );

  About = compose(
    withProps({
      getShapeSize: (y, x) => {
        const randomState = Math.random();
        const randomStateTwo = Math.random();
        const size = 1 + Math.sin(x * 20 + y * 20);
        return size * 15;
      },
    }),
    lifecycle({
      componentDidUpdate() {
        let hash = this.props.location.hash;
        if (hash) {
          let node = document.querySelector(hash);
          if (node) {
            node.scrollIntoView({ behavior: 'smooth' });
          }
        }
      },
    })
  )(AboutPure);
} else {
  About = () => (
    <RootStyle>
      <ContainerStyle>
        <FirstSection>
          <NavigationBar />
        </FirstSection>
        <StarField />
        <Footer />
      </ContainerStyle>
    </RootStyle>
  );
}

export default About;
