import React from 'react';
import { Switch, Route } from 'react-router-dom';

import AboutPage from '/both/imports/pages/About';
import SplashPage from '/both/imports/pages/Splash';
import TechnologyPage from '/both/imports/pages/Technology';

const Routes = () => (
  <Switch>
    <Route path="/" exact component={SplashPage} />
    <Route path="/about" exact component={AboutPage} />
    <Route path="/technology" exact component={TechnologyPage} />
    <Route
      render={() => (
        <div style={{ marginLeft: 50 }}>
          <h1>Page not found!</h1>
          <h2>I really need to make this look nicer...</h2>
        </div>
      )}
    />
  </Switch>
);

export default Routes;
