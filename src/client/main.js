import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { onPageLoad } from 'meteor/server-render';

onPageLoad(async (sink) => {
  const App = (await import('/both/imports/App')).App;
  ReactDOM.hydrate(
    <BrowserRouter>
      <App />
    </BrowserRouter>,
    document.getElementById('react-root')
  );
});
