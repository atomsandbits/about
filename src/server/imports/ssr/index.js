import React from 'react';
import PropTypes from 'prop-types';
import { renderToString } from 'react-dom/server';
import { onPageLoad } from 'meteor/server-render';
import { ServerStyleSheet } from 'styled-components';

import { App } from '/both/imports/App';
import { StaticRouter } from 'react-router';
import { Helmet } from 'react-helmet';

onPageLoad(async (sink) => {
  // console.time('Fetch Page');
  // const { loginToken } = sink.getCookies();

  // console.time('Page Render');
  const context = {};
  const sheet = new ServerStyleSheet();

  const FullApp = ({ disableStylesGeneration }) => (
    <StaticRouter location={sink.request.url} context={context}>
      <App />
    </StaticRouter>
  );
  FullApp.propTypes = {
    disableStylesGeneration: PropTypes.bool,
  };

  const html = renderToString(sheet.collectStyles(<FullApp />));

  // DOM
  sink.renderIntoElementById('react-root', html);
  // Tags
  const helmet = Helmet.renderStatic();
  sink.appendToHead(
    helmet.title
      .toString()
      .split(' data-react-helmet="true"')
      .join('')
  );
  sink.appendToHead(
    helmet.meta
      .toString()
      .split(' data-react-helmet="true"')
      .join('')
  );
  // Styles
  sink.appendToHead(sheet.getStyleTags());
  // console.timeEnd('Page Render');
  // console.timeEnd('Fetch Page');
});
